from sqlalchemy import create_engine
import sqlalchemy
import pandas as pd
import psycopg2 as pg
from datetime import timedelta
from math import ceil

sectors= ["'Manufacture of coke and refined petroleum products'",
"'Real estate activities'",
"'Forestry and logging'",
"'Crop and animal production, hunting and related service activities'",
"'Manufacture of other non-metallic mineral products'",
"'Wholesale trade, except of motor vehicles and motorcycles'",
"'Construction of buildings'",
"'Printing and reproduction of recorded media'",
"'Manufacture of basic metals'",
"'Activities of membership organisations'",
"'Activities of head offices; management consultancy activities'",
"'Manufacture of fabricated metal products, except machinery and equipment'",
"'Manufacture of basic pharmaceutical products '",
"'Wholesale and retail trade and repair of motor vehicles and motorcycles'",
"'Electricity, gas, steam and air conditioning supply'",
"'Manufacture of beverages'",
"'Information service activities'",
"'Rental and leasing activities'",
"'Education'",
"'Fishing and aquaculture'",
"'Accommodation'",
"'Specialised construction activities'",
"'Manufacture of electrical equipment'",
"'Publishing activities'",
"'Manufacture of furniture'",
"'Employment activities'",
"'Manufacture of food products'",
"'Extraction of crude petroleum and natural gas'",
"'Social work activities without accommodation'",
"'Creative, arts and entertainment activities'",
"'Human health activities'",
"'Financial service activities, except insurance and pension funding'",
"'Libraries, archives, museums and other cultural activities'",
"'Other personal service activities'",
"'Undifferentiated goods- and services-producing activities of private households for own use'",
"'Manufacture of wood and of products of wood'",
"'Other manufacturing'",
"'Motion picture, video and television programme production, sound recording and music publishing activities'",
"'Repair and installation of machinery and equipment'",
"'Mining support service activities'",
"'Gambling and betting activities'",
"'Manufacture of motor vehicles, trailers and semi-trailers'",
"'Activities auxiliary to financial services and insurance activities'",
"'Manufacture of rubber and plastic products'",
"'Office administrative, office support and other business support activities'",
"'Air transport'",
"'Legal and accounting activities'",
"'Retail trade, except of motor vehicles and motorcycles'",
"'Security and investigation activities'",
"'Manufacture of machinery and equipment'",
"'Remediation activities and other waste management services.'",
"'Water collection, treatment and supply'",
"'Manufacture of paper and paper products'",
"'Computer programming, consultancy and related activities'",
"'Land transport and transport via pipelines'",
"'Scientific research and development'",
"'Manufacture of chemicals and chemical products'",
"'Veterinary activities'",
"'Unknown'",
"'Manufacture of wearing apparel'",
"'Activities of extraterritorial organisations and bodies'",
"'Other professional, scientific and technical activities'",
"'Postal and courier activities'",
"'Waste collection, treatment and disposal activities; materials recovery'",
"'Mining of coal and lignite'",
"'Telecommunications'",
"'Insurance, reinsurance and pension funding, except compulsory social security'",
"'Food and beverage service activities'",
"'Warehousing and support activities for transportation'",
"'Architectural and engineering activities; technical testing and analysis'",
"'Public administration and defence; compulsory social security'",
"'Water transport'",
"'Repair of computers and personal and household goods'",
"'Programming and broadcasting activities'",
"'Manufacture of leather and related products'",
"'Manufacture of computer, electronic and optical products'",
"'Residential care activities'",
"'Sports activities and amusement and recreation activities'",
"'Services to buildings and landscape activities'",
"'Manufacture of other transport equipment'",
"'Manufacture of textiles'",
"'Civil engineering'",
"'Other mining and quarrying'",
"'Travel agency, tour operator and other reservation service and related activities'",
"'Advertising and market research'",
"'Sewerage'"]

class iris_engine:
    index_weight = .5
    iris_weight = .7
    g = .024
    engine = create_engine('postgresql+psycopg2://masteruser:W=!QnL8XbXqakMd=@dev-analytics.cnimcclfnoqr.eu-west-1.rds.amazonaws.com:5432/dwh')

    def read_sql(self, sql, **kwargs):
        return pd.read_sql(sql, con=self.engine, **kwargs)

    def indexewmoa(self,division):  # vp is the exponential moving average of one period in the past
        companydf = iris_engine.read_sql(self, 'select all_data_in.dt, all_data_in.company_abr,all_data_in.sic_division,  all_data_in.close_price, all_data_in.shares_issued from iris.all_data_in  where all_data_in.sic_division LIKE {} AND all_data_in.sic_division IS NOT NULL ;'.format(division))
        companydf['dt'] = pd.to_datetime(companydf['dt'])
        companydf['mcap'] = companydf['close_price'] * companydf['shares_issued'].astype('float64')
        indexdf = companydf.groupby('dt')['mcap'].agg('sum').reset_index()
        indexdf['emoa'] = None

        for i, cap in enumerate(indexdf['mcap']):
            if i == 0:
                indexdf['emoa'].iloc[i] = cap
            else:
                indexdf['emoa'].iloc[i] = self.index_weight * indexdf['mcap'].iloc[i - 1] + (1 - self.index_weight) * cap

        vn = indexdf['emoa'].loc[indexdf['dt'] == indexdf['dt'].max()].values[0]
        vhigh = indexdf['emoa'].max()
        return vn / vhigh

    def fuelconfunction(self, sicinput):
        sicfuelcondf = iris_engine.read_sql(self,'select * from iris.fuel_volume_sicdivision; where fuel_volume_sicdivision.sicdivision LIKE {} ;'.format(sicinput))
        try:
            sicfuelcondf['salesdatetime'] = pd.to_datetime(sicfuelcondf['salesdatetime'])
            fuelconi = sicfuelcondf['volume'].loc[sicfuelcondf['salesdatetime'] == sicfuelcondf['salesdatetime'].max()].values[0]
            fuelconp = sicfuelcondf['volume'].loc[sicfuelcondf['salesdatetime'] == sicfuelcondf['salesdatetime'].nlargest(2).iloc[-1]].values[0]
            fuelconhigh = sicfuelcondf['volume'].max()
            tdelta = (sicfuelcondf['salesdatetime'].max() - sicfuelcondf['salesdatetime'].loc[sicfuelcondf['volume'] == fuelconhigh].values[0]) / timedelta(weeks=1)
            temporal_scaler = fuelconhigh * tdelta ** -3
            current_ratio = fuelconi / fuelconp
            fc_relative_temporal_high = (fuelconi / fuelconhigh) * temporal_scaler
            return current_ratio - fc_relative_temporal_high
        except IndexError:
            return 0


    def iri(self, indexout, fuelconout):
        if 100 * ((indexout * self.iris_weight + fuelconout * (1 - self.iris_weight)) / (1 + self.g)) > 100:
            return 100
        else:
            return 100 * ((indexout * self.iris_weight + fuelconout * (1 - self.iris_weight)) / (1 + self.g))

    def bollingerbands(self, df):
        df['15daystd'] = df['iris_metric'].rolling(window=15).std()
        df['irisma'] = df['iris_metric'].rolling(window=15).mean()
        df['upper_band'] = df['irisma'] + (df['15daystd'] * 2)
        df['upper_band'].loc[df['upper_band']>100] = 100
        df['lower_band'] = df['irisma'] - (df['15daystd'] * 2)
        df['lower_band'].loc[df['lower_band']<0] = 0
        df = df.drop(columns=['15daystd','irisma'])
        df['upper_band'].fillna(df['iris_metric'], inplace=True)
        df['lower_band'].fillna(df['iris_metric'], inplace=True)
        return df

    def historical(self,division):
        companydf = iris_engine.read_sql(self, 'select all_data_in.dt, all_data_in.company_abr,all_data_in.sic_division,  all_data_in.close_price, all_data_in.shares_issued from iris.all_data_in  where all_data_in.sic_division LIKE {} AND all_data_in.sic_division IS NOT NULL ;'.format(division))
        companydf['dt'] = pd.to_datetime(companydf['dt'])
        companydf['mcap'] = companydf['close_price'] * companydf['shares_issued'].astype('float64')
        indexdf = companydf.groupby('dt')['mcap'].agg('sum').reset_index()
        indexdf.index=indexdf['dt']
        indexdf['emoa'] = None

        for i, cap in enumerate(indexdf['mcap']):
            if i == 0:
                indexdf['emoa'].iloc[i] = cap
            else:
                indexdf['emoa'].iloc[i] = self.index_weight * indexdf['emoa'].iloc[i - 1] + (1 - self.index_weight) * cap


        fueldf = iris_engine.read_sql(self, 'select * from iris.fuel_volume_sicdivision where fuel_volume_sicdivision.sicdivision LIKE {} ;'.format(division)).set_index('salesdatetime')
        indexfuel= pd.concat([indexdf,fueldf], join='inner',axis=1).drop(columns='mcap')

        indexfuel['indexmetric'] = None
        indexfuel['fuelmetric'] = None
        indexfuel['iris_metric'] = None

        for date in indexfuel.index:

            if date==indexfuel.index.min():
                indexfuel['indexmetric'].loc[date] = 1
                indexfuel['fuelmetric'].loc[date] = 1
                indexfuel['iris_metric'].loc[date] = 100
            else:
                fuelconi = indexfuel['volume'].loc[date]
                fuelconp = indexfuel['volume'].loc[indexfuel.index<=date].nlargest(2).iloc[0]
                fuelconhigh = indexfuel['volume'].loc[indexfuel.index<=date].max()
                tdelta = (date -indexfuel.index[indexfuel['volume'] == fuelconhigh]) / timedelta(weeks=1)
                if tdelta.values[0]==0:
                    temporal_scaler = 1
                else:
                    temporal_scaler = (fuelconhigh * ceil(tdelta.values[0])) ** -3
                current_ratio = fuelconi / fuelconp
                fc_relative_temporal_high = (fuelconi / fuelconhigh) * temporal_scaler
                if fuelconi==fuelconhigh:
                    indexfuel['fuelmetric'].loc[date] = 1
                else:
                    indexfuel['fuelmetric'].loc[date] = current_ratio - fc_relative_temporal_high

                indexfuel['indexmetric'].loc[date]= indexfuel['emoa'].loc[date]/indexfuel['emoa'].loc[indexfuel.index <= date].max()

                if  100 * ((indexfuel['indexmetric'].loc[date]* self.iris_weight + indexfuel['fuelmetric'].loc[date]* (1 - self.iris_weight)) / (1 + self.g)) > 100:
                    indexfuel['iris_metric'].loc[date] = 100
                else:
                    indexfuel['iris_metric'].loc[date] = 100 * ((indexfuel['indexmetric'].loc[date]* self.iris_weight + indexfuel['fuelmetric'].loc[date]* (1 - self.iris_weight)) / (1 + self.g))
        indexfuel = self.bollingerbands(indexfuel)

        return indexfuel

test= iris_engine()
added=pd.DataFrame()
for num,sic in enumerate(sectors):
    try:

        irisdf = test.historical(sic)
        added = added.append(irisdf, ignore_index=True)

    except ValueError:
        pass

# engine = create_engine('postgresql+psycopg2://masteruser:W=!QnL8XbXqakMd=@dev-analytics.cnimcclfnoqr.eu-west-1.rds.amazonaws.com:5432/dwh')
# added.to_sql('iris_metrics', engine, if_exists='replace', schema='iris',index=False)
# conn = engine.raw_connection()
# conn.commit()

